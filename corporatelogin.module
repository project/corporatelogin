<?php

/**
 * @file
 * Corporate Login module file.
 */
 
  /**
  * Returns the render array for the form.
 */

function corporatelogin_my_form($form, &$form_state) {
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('Please enter your corporate email address to login'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => 'Corporate Login'); 
  $form['#validate'] = array('corporatelogin_form_validation');
  return $form;
}

/**
 * Validates the form.
 */
function corporatelogin_form_validation($form, &$form_state) {
  $mail = $form_state['values']['email'];
  if (!valid_email_address($mail)) {
    form_set_error('email', t('Please Enter a valid email address.'));
  }
}

/**
 * Add a submit handler/function to the form.
 *
 * Fectch and check if the whether its is corporate email in
 * corporate_login_details table.
 *
 * If yes register and logged in automatically.
 */ 
function corporatelogin_my_form_submit($form, &$form_state) {
  $corpEmailID = $form_state['values']['email'];

  $corpEmailID_split = explode('@', $corpEmailID);
  $corpUserName = $corpEmailID_split[0];
  $corpUserDomain = $corpEmailID_split[1];
  $hasCorporateDomain = db_select('corporate_login_details', 'n')->fields('n')->condition('email', '%' . db_like($corpUserDomain) . '%', 'LIKE')->execute()->fetchAssoc();
  if ($hasCorporateDomain) {
    $checkExistsUsers = db_select('users', 'n')->fields('n')->condition('mail', $corpEmailID, '=')->execute()->fetchAssoc();
    if (!$checkExistsUsers) {
      global $user;
      $corp_user_info = array(
        'name' => $corpUserName,
        'pass' => user_password(),
        'roles' => array(DRUPAL_AUTHENTICATED_RID => TRUE),
        'mail' => $corpEmailID,
        'init' => $corpEmailID,
        'status' => 1,
        'access' => REQUEST_TIME,
        'timezone' => NULL,
      );
      $account = user_save(NULL, $corp_user_info);
      $op = 'password_reset';
      if ($account) {
        _user_mail_notify($op, $account);
      }
      drupal_set_message(t("An email has sent to your registered corporate ID. Please click on the link which is used to log in once and will lead you to a page where you can set your password."));
    }
    else {
      $user_obj = user_load_by_mail($corpEmailID);
      $form_state = array();
      $form_state['uid'] = $user_obj->uid;
      user_login_submit(array(), $form_state);
      drupal_set_message(t("You are now logged in as Corporate Account user!"));
      return TRUE;

    }
  }
  else {
    form_set_error('email', t('Sorry, you are not a Corporate Account user!'));
  }
 
}


/**
 * Implements hook_block_info().
 */
function corporatelogin_block_info() {
  $blocks['corporateuserlogin'] = array(
    'info' => t('Corporate User Login'),
    'status' => TRUE,
    'region' => 'content',
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'pages' => 'user/login',
  );
  return $blocks;  
}

/**
 * Implements hook_block_view().
 */ 
function corporatelogin_block_view($delta='') {
  $block = array();
  
  switch ($delta) {
    case 'corporateuserlogin':
      if (user_is_anonymous()) {
        $block['subject'] = t('Corporate User Login');
        $block['content'] = drupal_get_form('corporatelogin_my_form');
      }

      break;
  }
  return $block;
}

/**
 * Implements hook_menu().
 */
function corporatelogin_menu() {
  $items = array();
  // Admin Configuration setting.
  $items['admin/config/user-interface/corporate-account'] = array(
    'title' => 'Corporate Account | Configuration Settings',
    'description' => 'Configuring the Corporate Account Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('corporate_account_configuration_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/user-interface/corporate-account/add'] = array(
    'title' => 'Add Corporate Account',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/user-interface/corporate-account/manage'] = array(
    'title' => 'Manage Corporate Account',
    'description' => 'Managing the approved Corporate Accounts',
    'page callback' => 'manage_corporate_account',
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/user-interface/corporate-account/manage/delete/%'] =
  array(
    'title' => 'Delete Corporate Account',
    'description' => 'A page for admin to delete available Corporate Account',
    'page callback' => 'delete_corporate_account',
    'access arguments' => array('access administration pages'),
   ); 
  return $items;
}

/**
 * Corporate Account configuration.
  */
function corporate_account_configuration_form($form, &$form_state) {
  $form['corporate_account_management_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Corporate Account Management'),
    '#collapsible' => TRUE,
  );
  $form['corporate_account_management_settings']['corporate_account_admin_email'] =
  array(
    '#type' => 'select',
    '#title' => t('Domain Name'),
    '#description' => t('Choose the domain name which needs to be a Corporate Account'),
    '#options' => corporate_account_admin_email_options(),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
    '#attributes' => array('class' => array('btn', 'continue', 'proceed')),
  );
  return $form;
}

/**
 * Corporate Account configuration Form Submit.
 */ 
function corporate_account_configuration_form_submit($form, &$form_state) {
  $values = $form_state['input'];
  $created_date_time = new DateTime();
  $created_date_time_format = $created_date_time->format('Y-m-d H:i:s');
  $hasEmail = db_select('corporate_login_details', 'n')->fields('n')->condition('email', $values['corporate_account_admin_email'], '=')->execute()->fetchAssoc();
  if (!$hasEmail) {
    $corporate_account_save = db_insert('corporate_login_details')->fields(array('email' => $values['corporate_account_admin_email'], 'created' => $created_date_time_format))->execute();
    drupal_set_message(t("Record Added Successfully!"));
    drupal_goto('admin/config/user-interface/corporate-account/manage');
  }
  else {
    form_set_error('email', t('Record already Exist!'));
  }
 }

/**
 * Corporate Account configuration - Fetch email from database.
 */
 
function corporate_account_admin_email_options() {
  $rid = array_search('corporate', user_roles());
  $stmt = 'SELECT DISTINCT(ur.uid)
 	  FROM {users_roles} AS ur
 	  WHERE ur.rid IN (:rids)';
  $result = db_query($stmt, array(':rids' => $rid));
  $uids = $result->fetchCol();
  $options = array();
  $options[NULL] = "- Any -";
  foreach ($uids as $row) {
    $users = user_load($row);
    $caEmailID = $users->mail;
 
    $caEmailID_split = explode('@', $caEmailID);
    $caUserDomain = $caEmailID_split[1];
    $options[$caUserDomain] = $caUserDomain;
  }
  return $options;
}


/**
 * Corporate Account configuration - Fetch role by name.
 */ 
function get_role_by_name($name) {
  $roles = user_roles();
  return array_search($name, $roles);
}

/**
 * Implements hook_help().
 */
function corporatelogin_help($path, $arg) {
  switch ($path) {
    case 'admin/help#corporatelogin':
      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];
        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }
	  
      return $output;
  }
}

/**
 * Implements manage_corporate_account().
 */
 
function manage_corporate_account() {
  $approved_corporate_account_array = db_query("SELECT * FROM {corporate_login_details}")->fetchAll();
  $header = array(t('ID'), t('Email'), t('Created Date'), t('Delete'));
  $rows = array();
  foreach ($approved_corporate_account_array as $result) {
    $rows[] = array(
      $result->id,
      $result->email,
      $result->created,
      l(t('Delete'), 'admin/config/user-interface/corporate-account/manage/delete/' . $result->id, array('attributes' => array('class' => array('delete_cocktails')))),
    );
  }
  if (!empty($rows)) {
    $output = theme('table', array('header' => $header, 'rows' => $rows));
    return $output;
  }
  else {
    return 'No records added yet!!';
  }
}

/**
 * Implements delete_corporate_account().
 */ 
function delete_corporate_account() {
  $cid = arg(6);
  $num_deleted = db_delete('corporate_login_details')->condition('id', $cid)->execute();
  drupal_set_message(t('Selected Corporate Account has been deleted successfully'));
  drupal_goto('admin/config/user-interface/corporate-account/manage');
}